<?php

    use App\Models\Student;
    use Illuminate\Support\Facades\Route;

    Route::get('/students', function () {
        return Student::all();
    });

    Route::get('/students/add/{name}/{age}/{major}', function ($name, $age, $major) {
        return Student::create([
            'name' => $name,
            'age' => $age,
            'major' => $major,
        ]);
    });

    Route::get('/students/{id}', function ($id) {
        return Student::findOrFail($id);
    });

    Route::get('/students/find/{name}', function ($name) {
        return Student::where('name', $name)
            ->orderBy('age')
            ->get();
    });

    Route::get('/students/{id}/update/{age}', function ($id, $age) {
        Student::where('id', $id)
            ->update([
                'age' => $age,
            ]);
    });

    Route::get('/students/{id}/delete', function ($id) {
        Student::findOrFail($id)->delete();
    });
